**User Authentication and Authorization System**
==============================================

This project implements a standalone module for user registration, authentication, and login processes using Node.js as the backend technology and MongoDB as the database. The system allows individuals to register by providing personal information, including name, family name, national code, phone number, password, and user role (normal user or administrator). The registered user's information is stored in a MongoDB collection.

### Web Services

The system provides three main web services:

#### 1. User Registration

Enables users to create an account by providing required information.

#### 2. User Login

Authenticates users using their phone number and password, and upon successful authentication, issues a token containing essential information such as phone number, national code, and user role.

#### 3. User Information Edit

Allows administrators to edit user information (accessible only to users with the administrator role).

### Authentication and Authorization

The generated token enables access control based on the user's role, ensuring that users can only access authorized areas of the system. This project provides a robust foundation for user authentication and authorization, ensuring secure and controlled access to the system's features and resources.