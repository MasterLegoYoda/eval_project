# API Documentation
=====================

## Register User

### Endpoint: `/register`
### Method: `POST`

Register a new user.

#### Request Body

```json
{
  "name": "string",
  "familyName": "string",
  "nationalCode": "string",
  "phoneNumber": "string",
  "password": "string",
  "userRole": "string"
}
```

#### Response

* `201 Created`: User registered successfully.
* `400 Bad Request`: Validation error or a user with the same national code already exists.
* `500 Internal Server Error`: Server error occurred.

## Login User

### Endpoint: `/login`
### Method: `POST`

Log in an existing user.

#### Request Body

```json
{
  "phoneNumber": "string",
  "password": "string"
}
```

#### Response

* `200 OK`: User logged in successfully.
* `401 Unauthorized`: Invalid phone number or password.
* `500 Internal Server Error`: Server error occurred.

## Edit User

### Endpoint: `/users`
### Method: `PUT`

Edit an existing user's details by an administrator.

#### Request Body

```json
{
  "phoneNumber": "string",
  "name": "string",
  "familyName": "string",
  "nationalCode": "string"
}
```

#### Response

* `200 OK`: User details updated successfully.
* `403 Forbidden`: User is not an administrator.
* `500 Internal Server Error`: Server error occurred.
