const jwt = require('jsonwebtoken');

function authenticateToken(req, res, next) {
  const token = req.headers.authorization?.split(' ')[1];
  if (!token) {
    res.statusCode = 401;
    res.end('Unauthorized');
    return;
  }
  jwt.verify(token, process.env.JWT_SECRET, function(err, decoded) {
    if (err) {
      res.statusCode = 401;
      res.end('Unauthorized');
      return;
    }
    req.user = decoded;
    next();
  });
}

module.exports = { authenticateToken };