const { MongoClient, ServerApiVersion } = require('mongodb');

const uri = process.env.DATABASE_URL;
const dbName = process.env.DATABASE_NAME;
const client = new MongoClient(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  serverApi: ServerApiVersion.v1
});

let db;
const dbConnected = client.connect().then(() => {
  db = client.db(dbName);
  console.log("Connected to MongoDB and selected database:", dbName);
}).catch(err => {
  console.error("Failed to connect to MongoDB:", err);
  process.exit(1);
});

async function getDb() {
  await dbConnected;
  return db;
}

module.exports = getDb;