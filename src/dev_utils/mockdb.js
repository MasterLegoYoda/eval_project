// mockdb.js
class MockDB {
    constructor() {
      this.users = []; // This will hold user data in memory
    }
  
    collection(collectionName) {
      // We only have a 'users' collection in this mock, but you can extend it to other collections if needed.
      if (collectionName === 'users') {
        return {
          // Mimic the insertOne function
          insertOne: (user, callback) => {
            user._id = this.users.length + 1; // Simple incrementing ID for demonstration purposes
            this.users.push(user);
            callback(null, { insertedId: user._id }); // Simulate successful insertion with no error
          },
          // Mimic the findOne function
          findOne: (query, callback) => {
            const user = this.users.find(u => u.phoneNumber === query.phoneNumber);
            callback(null, user || null); // Return null if no user is found to mimic MongoDB's behavior
          },
          // Mimic the updateOne function
          updateOne: (query, update, callback) => {
            const userIndex = this.users.findIndex(u => u.phoneNumber === query.phoneNumber);
            if (userIndex === -1) {
              callback(null, { matchedCount: 0 }); // No user found to update
            } else {
              this.users[userIndex] = { ...this.users[userIndex], ...update.$set };
              callback(null, { matchedCount: 1, modifiedCount: 1 }); // Simulate successful update
            }
          }
        };
      }
      throw new Error('Collection not found'); // If an unsupported collection is requested
    }
  }
  
  // Export a singleton instance of MockDB
  module.exports = new MockDB();