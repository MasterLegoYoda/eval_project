const Joi = require('joi');
const auth = require('../model/auth');

async function editUser(req, res, db) {
  auth.authenticateToken(req, res, async () => {
    if (req.user.userRole !== 'administrator') {
      res.statusCode = 403;
      res.end('Forbidden');
      return;
    }

    let body = '';
    req.on('data', chunk => {
      body += chunk.toString();
    });
    req.on('end', async () => {
      const schema = Joi.object({
        phoneNumber: Joi.string().length(11).pattern(/^[0-9]+$/).required(),
        name: Joi.string().min(2).optional(),
        familyName: Joi.string().min(2).optional(),
        nationalCode: Joi.string().length(10).pattern(/^[0-9]+$/).optional()
      });

      const { error, value } = schema.validate(JSON.parse(body));

      if (error) {
        res.statusCode = 400;
        res.end(`Validation error: ${error.details.map(x => x.message).join(', ')}`);
        return;
      }

      try {
        const result = await db.collection('users').updateOne(
          { phoneNumber: value.phoneNumber },
          { $set: value }
        );
        if (result.matchedCount === 0) {
          res.statusCode = 404;
          res.end('User not found');
          return;
        }
        res.statusCode = 200;
        res.end('User updated');
      } catch (err) {
        console.error(err);
        res.statusCode = 500;
        res.end('Failed to update user');
      }
    });
  });
}

module.exports = editUser;