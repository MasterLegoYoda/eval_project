const bcrypt = require('bcrypt');
const Joi = require('joi');

async function registerUser(req, res, db) {
    let body = '';
    req.on('data', chunk => {
        body += chunk.toString();
    });
    req.on('end', async () => {
        const schema = Joi.object({
            name: Joi.string().min(2).required(),
            familyName: Joi.string().min(2).required(),
            nationalCode: Joi.string().length(10).pattern(/^[0-9]+$/).required(),
            phoneNumber: Joi.string().length(11).pattern(/^[0-9]+$/).required(),
            password: Joi.string().min(6).required(),
            userRole: Joi.string().valid('normal user', 'administrator').required()
        });

        const { error, value } = schema.validate(JSON.parse(body));

        if (error) {
            res.statusCode = 400;
            res.end(`Validation error: ${error.details.map(x => x.message).join(', ')}`);
            return;
        }

        try {
            const existingUser = await db.collection('users').findOne({ nationalCode: value.nationalCode });
            if (existingUser) {
                res.statusCode = 400;
                res.end('A user with the same national code already exists');
                return;
            }

            const hash = await bcrypt.hash(value.password, 10);
            const user = {
                name: value.name,
                familyName: value.familyName,
                nationalCode: value.nationalCode,
                phoneNumber: value.phoneNumber,
                password: hash,
                userRole: value.userRole
            };
            await db.collection('users').insertOne(user);
            res.statusCode = 201;
            res.end('User registered');
        } catch (err) {
            console.error(err);
            res.statusCode = 500;
            res.end('Server error');
        }
    });
}

module.exports = registerUser;