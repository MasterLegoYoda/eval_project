const bcrypt = require('bcrypt');
const Joi = require('joi');
const jwt = require('jsonwebtoken');

async function loginUser(req, res, db) {
    let body = '';
    req.on('data', chunk => {
        body += chunk.toString();
    });
    req.on('end', async () => {
        const schema = Joi.object({
            nationalCode: Joi.string().length(10).pattern(/^[0-9]+$/).required(),
            password: Joi.string().min(6).required()
        });

        const { error, value } = schema.validate(JSON.parse(body));

        if (error) {
            res.statusCode = 400;
            res.end(`Validation error: ${error.details.map(x => x.message).join(', ')}`);
            return;
        }

        try {
            const user = await db.collection('users').findOne({ nationalCode: value.nationalCode });
            if (!user || !(await bcrypt.compare(value.password, user.password))) {
                res.statusCode = 401;
                res.end('Invalid credentials');
                return;
            }

            const token = jwt.sign(
                { userId: user._id, userRole: user.userRole },
                process.env.JWT_SECRET,
                { expiresIn: '1h' }
            );

            res.statusCode = 200;
            res.end(JSON.stringify({ token }));
        } catch (err) {
            console.error(err);
            res.statusCode = 500;
            res.end('Server error');
        }
    });
}

module.exports = loginUser;