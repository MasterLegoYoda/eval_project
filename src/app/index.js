require('dotenv').config();
const http = require('http');
const url = require('url');
const getDb = require('../model/db');
const registerUser = require('../controllers/register');
const loginUser = require('../controllers/login');
const editUser = require('../controllers/editUser');

getDb().then(db => {
  const server = http.createServer((req, res) => {
    const { pathname } = url.parse(req.url);
    const { method } = req;

    if (method === 'POST' && pathname === '/register') {
      registerUser(req, res, db);
    } else if (method === 'POST' && pathname === '/login') {
      loginUser(req, res, db);
    } else if (method === 'PUT' && pathname === '/users') {
      editUser(req, res, db);
    } else {
      res.statusCode = 404;
      res.end('Not Found');
    }
  });

  server.listen(3000, () => {
    console.log('Server started on port 3000');
  });
}).catch(err => {
  console.error('Failed to start server:', err);
});